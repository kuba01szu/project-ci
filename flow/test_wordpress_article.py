import time
from base64 import b64encode
import requests
import pytest
import lorem


# GLOBAL VARIABLES
editor_username = 'editor'
editor_email = 'editor@somesite.com'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")

commenter_username = 'commenter'
commenter_email = 'commenter@somesite.com'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
blog_url = 'https://gaworski.net'
comments_endpoint_url = blog_url + '/wp-json/wp/v2/comments'
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def timestamp():
    return str(int(time.time))


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
       "article_creation_date": timestamp,
       "article_title": "This is new post JS " + str(timestamp),
       "article_subtitle": lorem.sentence(),
       "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def comment():

    comment = {
        "comment_creation_date": timestamp,
        "comment_title": "This is new comment JS " + str(timestamp),
        "comment_subtitle": lorem.sentence(),
        "comment_text": lorem.paragraph()
    }
    return comment


@pytest.fixture(scope='module')
def headers():
    headers = {
       "Content-Type": "application/json",
       "Authorization": "Basic " + editor_token
    }
    return headers


@pytest.fixture(scope='module')
def posted_article(article, headers):
    payload = {
       "title": article["article_title"],
       "excerpt": article["article_subtitle"],
       "content": article["article_text"],
       "status": "publish"
    }
    print(posts_endpoint_url)
    response = requests.post(url=posts_endpoint_url, headers=headers, json=payload)
    return response


@pytest.fixture(scope='module')
def commenter_headers():
    commenter_headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return commenter_headers


@pytest.fixture(scope='module')
def posted_comment(article, comment, commenter_headers):
    payload = {
       "title": comment["comment_title"],
       "excerpt": comment["comment_subtitle"],
       "content": comment["comment_text"],
       "status": "publish"
    }
    print(comments_endpoint_url)
    response = requests.post(url=posts_endpoint_url, headers=commenter_headers, json=payload)
    return response


@pytest.fixture(scope='module')
def reply(headers):
    reply = {
        "reply_creation_date": timestamp,
        "reply_title": "This is reply Js " + str(timestamp),
        "reply_subtitle": lorem.sentence(),
        "reply_text": lorem.paragraph()
    }
    return reply


@pytest.fixture(scope='module')
def posted_reply(reply, comment, headers):
    payload = {
       "title": reply["reply_title"],
       "excerpt": reply["reply_subtitle"],
       "content": reply["reply_text"],
       "status": "publish"
    }
    print(comments_endpoint_url)
    response = requests.post(url=comments_endpoint_url, headers=headers, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_new_comment_is_successfully_created(posted_article, posted_comment, comment, commenter_headers):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_comment = requests.get(url=wordpress_post_url)

    assert published_comment.status_code == 200
    assert published_comment.reason == "OK"


def test_new_post_can_be_read(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'


def test_user_create_first_comment_to_article_and_reply_to_comment(posted_article, comment, reply, commenter_headers, headers):

    url = blog_url + '/wp-json/wp/v2/comments'
    wordpress_post_id = posted_article.json()["id"]

    comment_data = {
        "title": comment["comment_title"],
        "excerpt": comment["comment_subtitle"],
        'post': int(wordpress_post_id),
        "content": comment["comment_text"],
        "status": "approved"
    }

    response_comment = requests.post(url=url, json=comment_data, headers=commenter_headers)
    assert response_comment.status_code == 201
    assert response_comment.reason == "Created"

    comment_id = response_comment.json()["id"]
    url_get = url + "/" + str(comment_id)

    response_comment_get = requests.get(url=url_get, headers=headers)
    assert response_comment_get.status_code == 200
    assert response_comment_get.reason == "OK"

    received_data = response_comment_get.json()
    assert received_data['author'] == 3
    assert received_data['status'] == 'approved'
    assert received_data['author_name'] == 'Albert Commenter'

    reply_data = {
        "title": reply["reply_title"],
        "excerpt": reply["reply_subtitle"],
        'post': int(wordpress_post_id),
        'parent': int(comment_id),
        "content": reply["reply_text"],
        "status": "approved"
    }

    response_reply = requests.post(url=comments_endpoint_url, json=reply_data, headers=headers)
    assert response_reply.status_code == 201
    assert response_reply.reason == "Created"

    reply_id = response_reply.json()["id"]
    url_reply = url + "/" + str(reply_id)
    response_reply_get = requests.get(url=url_reply, headers=headers)
    assert response_reply_get.status_code == 200
    assert response_reply_get.reason == 'OK'

    received_reply_data = response_reply_get.json()
    assert received_reply_data['author'] == 2
    assert received_reply_data['author_name'] == 'John Editor'
    assert reply_data['parent'] == comment_id


